package explosions.server;

import com.jme3.bullet.BulletAppState;
import com.jme3.bullet.collision.PhysicsCollisionObject;
import com.jme3.bullet.collision.shapes.BoxCollisionShape;
import com.jme3.bullet.collision.shapes.CollisionShape;
import com.jme3.bullet.collision.shapes.SphereCollisionShape;
import com.jme3.bullet.control.KinematicRagdollControl;
import com.jme3.bullet.control.RigidBodyControl;
import com.jme3.bullet.joints.HingeJoint;
import com.jme3.math.Vector3f;
import com.jme3.scene.Geometry;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import explosions.Broadcaster;
import explosions.Factory;
import explosions.GameObjectFinder;
import static explosions.server.RocketControl.ROCKET_COLLISION_RADIUS;
import static explosions.server.RocketControl.ROCKET_MASS;
import static explosions.server.RocketControl.ROCKET_VELOCITY;
import explosions.server.entity.BotControl;
import explosions.server.entity.EntityUtility;
import explosions.server.entity.HumanControl;
import explosions.server.entity.ItemControl;
import explosions.server.entity.PlayerControl;
import explosions.server.entity.SpawnControl;
import explosions.server.entity.SpawnStrategy;
import explosions.server.entity.UpdateControl;
import explosions.server.message.ServerMessage.SpawnMessage;
import explosions.server.weapons.Pistol;
import explosions.server.weapons.PistolShooter;
import explosions.server.weapons.RocketLauncher;
import explosions.server.weapons.RocketShooter;
import explosions.server.weapons.Shooter;

/**
 *
 * @author Roland Fuller
 */
public class ServerFactory extends Factory {

    public static final String SOLID = "SOLID";
    private BulletAppState bulletAppState;
    private EntityUtility entityUtility;
    private WorldUtility worldUtility;
    private int entityId = 0;

    public ServerFactory(WorldUtility worldUtility, EntityUtility entityUtility, BulletAppState bulletAppState, Broadcaster broadcaster, Node rootNode) {
        super(rootNode, broadcaster);
        this.bulletAppState = bulletAppState;
        this.entityUtility = entityUtility;
        this.worldUtility = worldUtility;
    }

    private RigidBodyControl givePhysics(float mass, Spatial spatial, CollisionShape collisionShape) {
        RigidBodyControl rigidBodyControl = null;
        if (collisionShape == null) {
            rigidBodyControl = new RigidBodyControl(mass);
        } else {
            rigidBodyControl = new RigidBodyControl(collisionShape, mass);
        }
        spatial.addControl(rigidBodyControl);
        bulletAppState.getPhysicsSpace().add(rigidBodyControl);
        return rigidBodyControl;
    }

    public Node makeWorld(Node worldRoot) {
        rootNode.attachChild(worldRoot);
        worldRoot.setUserData(ID, ++entityId);
        worldRoot.setName(MAP);
        for (Spatial component : ((Node) worldRoot.getChildren().get(0)).getChildren()) {
            final String name = component.getName().toUpperCase();
            if (name.contains(SOLID)) {
                givePhysics(0, component, null);
            } else if (name.contains(PLAYER_SPAWNER)) {
                makeBot(component.getLocalTranslation());
            } else if (name.contains(AMMO_SPAWNER)) {
                makeSpawner(component.getLocalTranslation(), new SpawnStrategy.AmmoStrategy(this));
            }
        }

        for (Spatial spatial : GameObjectFinder.FindByType(worldRoot, Geometry.class)) {
            spatial.removeFromParent();
        }
        return worldRoot;
    }

    public Node makeRocket(Vector3f location, Vector3f direction, float spawnPoint) {
        Vector3f offset = direction.mult(5).add(0, spawnPoint, 0);
        Node container = makeCore(location.add(offset), ROCKET, ++entityId);
        RigidBodyControl rigidBodyControl = givePhysics(ROCKET_MASS, container, new SphereCollisionShape(ROCKET_COLLISION_RADIUS));
        rigidBodyControl.setLinearVelocity(direction.mult(ROCKET_VELOCITY));
        rigidBodyControl.setGravity(Vector3f.ZERO);
        container.addControl(new RocketControl());
        container.addControl(new UpdateControl(broadcaster));
        broadcaster.send(new SpawnMessage(entityId, ROCKET, location));
        return container;
    }

    public Node makeHuman(Vector3f location) {
        final HumanControl humanControl = new HumanControl(entityUtility);
        giveWeapons(humanControl);
        return makePlayer(location, humanControl);
    }

    public Node makeBot(Vector3f location) {
        final BotControl botControl = new BotControl(entityUtility);
        giveWeapons(botControl);
        return makePlayer(location, botControl);
    }

    private Node makePlayer(Vector3f location, PlayerControl control) {
        Node container = makeCore(location, PLAYER, ++entityId);
        final UpdateControl updateControl = new UpdateControl(broadcaster);
        container.addControl(updateControl);
        control.setUpdateControl(updateControl);
        container.addControl(control);
        bulletAppState.getPhysicsSpace().add(container);
        return container;
    }

    private void giveWeapons(PlayerControl control) {
        Shooter rocketShooter = new RocketShooter(this, control);
        Shooter bulletShooter = new PistolShooter(this, control);
        final RocketLauncher rocketLauncher = new RocketLauncher(rocketShooter, control.getCache());
        control.giveWeapon(rocketLauncher);
        final Pistol pistol = new Pistol(bulletShooter, control.getCache());
        control.giveWeapon(pistol);
    }

    public void makeBullet(Vector3f location, Vector3f viewDirection, PhysicsCollisionObject physicsCollisionObject, float spawnPoint) {
        location.addLocal(new Vector3f(0, spawnPoint, 0));
        worldUtility.rayCast(location, viewDirection, physicsCollisionObject, new RayMessage(100, 25, 500));
    }

    public void makeAmmo(Vector3f location, SpawnControl spawnControl) {
        Node container = makeCore(location, AMMO, ++entityId);
        givePhysics(0, container, new BoxCollisionShape(new Vector3f(1, 1, 1)));
        container.addControl(new UpdateControl(broadcaster));
        container.addControl(new ItemControl(spawnControl));
        broadcaster.send(new SpawnMessage(entityId, AMMO, location));
    }

    public Node makeSpawner(Vector3f location, SpawnStrategy strategy) {
        Node container = makeCore(location, AMMO_SPAWNER, ++entityId);
        container.addControl(new SpawnControl(strategy));
        return container;
    }

    private RigidBodyControl makeChainLink(Vector3f location) {
        Node container = makeCore(location, CHAIN, ++entityId);
        CollisionShape collisionShape = new SphereCollisionShape(0.5f);
        RigidBodyControl rbc = givePhysics(1, container, collisionShape);
       // rootNode.attachChild(container);
      //  bulletAppState.getPhysicsSpace().add(container);
        return rbc;
    }

    public void makeChain(Vector3f location, int numLinks) {
        RigidBodyControl rbcA = makeChainAnchor(location);
        for (int i = 0; i < numLinks; i++) {
            RigidBodyControl rbcB = makeChainLink(location.add(Vector3f.UNIT_Y.mult(i)));
            HingeJoint joint = new HingeJoint(rbcA, // A
                    rbcB, // B
                    new Vector3f(0f, 0f, 0f), // pivot point local to A
                    new Vector3f(0f, 1f, 0f), // pivot point local to B
                    Vector3f.UNIT_XYZ, // DoF Axis of A
                    Vector3f.UNIT_XYZ);            // DoF Axis of B 

            bulletAppState.getPhysicsSpace().add(joint);
            rbcA = rbcB;
        }
    }

    private RigidBodyControl makeChainAnchor(Vector3f location) {
        Node container = makeCore(location, CHAIN, ++entityId);
        RigidBodyControl rbc = givePhysics(0, container, new SphereCollisionShape(0.1f));
        rootNode.attachChild(container);
        bulletAppState.getPhysicsSpace().add(rbc);
        return rbc;
    }
}
