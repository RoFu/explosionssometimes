package explosions.server.entity;

import com.jme3.renderer.RenderManager;
import com.jme3.renderer.ViewPort;
import com.jme3.scene.control.AbstractControl;

/**
 *
 * @author Roland Fuller
 */
public class ItemControl extends AbstractControl {

    private SpawnControl spawnControl;

    public ItemControl(SpawnControl spawnControl) {
        this.spawnControl = spawnControl;
    }

    @Override
    protected void controlUpdate(float tpf) {
    }

    @Override
    protected void controlRender(RenderManager rm, ViewPort vp) {
    }

    public void take() {
        spawnControl.spawnedTaken();
    }

}
