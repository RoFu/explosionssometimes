package explosions.server.entity;

import com.jme3.scene.Spatial;

/**
 *
 * @author Roland Fuller
 */
public interface EntityUtility {

    void die(Spatial spatial);

    void rag(Spatial spatial);
}
