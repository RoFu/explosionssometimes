package explosions.server.entity;

import com.jme3.bullet.collision.PhysicsCollisionObject;
import com.jme3.bullet.control.BetterCharacterControl;
import com.jme3.bullet.control.KinematicRagdollControl;
import com.jme3.bullet.objects.PhysicsRigidBody;
import com.jme3.math.Vector3f;
import static explosions.client.entity.PlayerControl.BACKWARD;
import static explosions.client.entity.PlayerControl.FORWARD;
import static explosions.client.entity.PlayerControl.JUMP;
import static explosions.client.entity.PlayerControl.LEFT;
import static explosions.client.entity.PlayerControl.RELOAD;
import static explosions.client.entity.PlayerControl.RIGHT;
import static explosions.client.entity.PlayerControl.SHOOT;
import static explosions.client.entity.PlayerControl.SWITCH;
import explosions.server.RayMessage;
import explosions.server.weapons.Cache;
import explosions.server.weapons.Weapon;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Roland Fuller
 */
public abstract class PlayerControl extends BetterCharacterControl {

    private static final int PLAYER_SPEED = 10;
    private boolean doJump;
    private boolean goForward;
    private boolean goBackward;
    private boolean doShoot;
    private boolean doReload;
    private boolean doSwitch;
    private boolean goRight;
    private boolean goLeft;
    private Weapon currentWeapon;
    private List<Weapon> weapons;
    private Cache cache;
    private int health;
    private EntityUtility entityUtil;
    private UpdateControl updateControl;

    public PlayerControl(EntityUtility entityUtil) {
        super(1, 4, 1);
        this.entityUtil = entityUtil;
        weapons = new ArrayList<>();
        cache = new Cache(10000);
        health = 100;
        setJumpForce(new Vector3f(0, mass * 2, 0));
    }

    public void setUpdateControl(UpdateControl updateControl) {
        this.updateControl = updateControl;
    }

    public void hurt(int amount) {
        health -= amount;
        if (health <= 0) {
            //entityUtil.die(spatial);
            entityUtil.rag(spatial);
        }
    }

    public void addHealth(int amount) {
        health += amount;
    }

    public Cache getCache() {
        return cache;
    }

    public void giveWeapon(Weapon weapon) {
        if (weapons.isEmpty()) {
            currentWeapon = weapon;
        }
        weapons.add(weapon);
    }

    public void action(String command, boolean pressed) {
        switch (command) {
            case JUMP:
                doJump = pressed;
                break;
            case FORWARD:
                goForward = pressed;
                break;
            case BACKWARD:
                goBackward = pressed;
                break;
            case LEFT:
                goLeft = pressed;
                break;
            case RIGHT:
                goRight = pressed;
                break;
            case SHOOT:
                doShoot = pressed;
                break;
            case RELOAD:
                doReload = pressed;
                break;
            case SWITCH:
                doSwitch = pressed;
                break;
        }
    }

    @Override
    public void update(float tpf) {
        super.update(tpf);

        /**
         * Fixed wall sticking bug #10 In the air, the player is frictionless.
         */
        if (!isOnGround()) {
            ((PhysicsRigidBody) getRigidBody()).setFriction(0);
        } else {
            ((PhysicsRigidBody) getRigidBody()).setFriction(0.5f);
        }

        currentWeapon.update(tpf);
        Vector3f direction = new Vector3f();
        if (doJump) {
            updateControl.playAnimation("Dodge");
            jump();
        }
        if (doShoot) {
            currentWeapon.fire();
            updateControl.playSound("Sound/Effects/Bang.wav");
            doShoot = false;
        }
        if (doReload) {
            currentWeapon.reload();
            doReload = false;
        }
        if (doSwitch) {
            int index = weapons.indexOf(currentWeapon);
            index++;
            if (index > weapons.size() - 1) {
                index = 0;
            }
            currentWeapon = weapons.get(index);
            doSwitch = false;
        }
        if (goForward) {
            updateControl.playAnimation("Walk");
            direction.addLocal(viewDirection);
        }
        if (goBackward) {
            updateControl.playAnimation("Walk");
            direction.addLocal(viewDirection.negate());
        }
        if (goLeft) {
            updateControl.playAnimation("Walk");
            direction.addLocal(viewDirection.cross(Vector3f.UNIT_Y.negate()));
        }
        if (goRight) {
            updateControl.playAnimation("Walk");
            direction.addLocal(viewDirection.cross(Vector3f.UNIT_Y));
        }
        // TODO: Currently this breaks the animation completely. Need to figure out
        // why that is to fix it.
//        if (!(goForward && goBackward && goLeft && goRight)) {
//            updateControl.playAnimation("stand");
//        }
        direction.normalizeLocal().multLocal(PLAYER_SPEED).multLocal(new Vector3f(1, 0, 1));
        setWalkDirection(direction);
    }

    public Vector3f getLocation() {
        return spatial.getLocalTranslation();
    }

    public float getHeight() {
        return getFinalHeight();
    }

    public void look(Vector3f lookDirection) {
        setViewDirection(lookDirection);
    }

    public void hit(RayMessage rayMessage) {
        hurt(rayMessage.getDamage());
    }

    public PhysicsCollisionObject getRigidBody() {
        return rigidBody;
    }

    public void giveAmmo(int i) {
        cache.giveAmmo(i);
    }
}
