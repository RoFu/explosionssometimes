/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package explosions.server.message;

import com.jme3.math.Vector3f;
import com.jme3.network.serializing.Serializable;
import explosions.NetworkMessage;

/**
 *
 * @author Roland Fuller
 */
public abstract class ServerMessage extends NetworkMessage {

    @Serializable
    public static class AudioMessage extends ServerMessage {

        public int id;
        public String effect;

        public AudioMessage() {
        }

        public AudioMessage(int id, String effect) {
            this.id = id;
            this.effect = effect;
        }
    }

    @Serializable
    public static class UpdateLocationMessage extends ServerMessage {

        public int id;
        public Vector3f location;

        public UpdateLocationMessage() {
        }

        public UpdateLocationMessage(int id, Vector3f location) {
            this.id = id;
            this.location = location;
        }

    }

    @Serializable
    public static class RemoveMessage extends ServerMessage {

        public int id;

        public RemoveMessage() {
        }

        public RemoveMessage(int id) {
            this.id = id;
        }
    }

    @Serializable
    public static class AnimationMessage extends ServerMessage {

        public int id;
        public String animation;

        public AnimationMessage() {
        }

        public AnimationMessage(int id, String animation) {
            this.id = id;
            this.animation = animation;
        }
    }

    @Serializable
    public static class SpawnMessage extends ServerMessage {

        public int id;
        public String gameObject;
        public Vector3f location;
        public boolean isPlayer;

        public SpawnMessage() {
        }

        public SpawnMessage(int id, String gameObject, Vector3f location) {
            this(id, gameObject, location, false);
        }

        public SpawnMessage(int id, String gameObject, Vector3f location, boolean isPlayer) {
            this.id = id;
            this.gameObject = gameObject;
            this.location = location;
            this.isPlayer = isPlayer;
        }

    }
}
