package explosions.server;

import com.jme3.app.Application;
import com.jme3.app.state.AppStateManager;
import com.jme3.bullet.BulletAppState;
import com.jme3.bullet.collision.PhysicsCollisionEvent;
import com.jme3.bullet.collision.PhysicsCollisionListener;
import com.jme3.bullet.collision.PhysicsCollisionObject;
import com.jme3.bullet.collision.PhysicsRayTestResult;
import com.jme3.bullet.control.KinematicRagdollControl;
import com.jme3.math.Vector3f;
import com.jme3.network.ConnectionListener;
import com.jme3.network.Filters;
import com.jme3.network.HostedConnection;
import com.jme3.network.Message;
import com.jme3.network.MessageListener;
import com.jme3.network.Network;
import com.jme3.network.Server;
import com.jme3.scene.Node;
import com.jme3.scene.SceneGraphVisitor;
import com.jme3.scene.Spatial;
import explosions.AppState;
import explosions.Broadcaster;
import explosions.Factory;
import static explosions.Factory.AMMO;
import static explosions.Factory.ID;
import static explosions.Factory.MAP;
import static explosions.Factory.PLAYER;
import static explosions.Factory.ROCKET;
import explosions.GameObjectFinder;
import explosions.NetworkMessage;
import explosions.client.message.ClientMessage;
import explosions.client.message.ClientMessage.ChatMessage;
import explosions.client.message.ClientMessage.PlayerInputMessage;
import explosions.client.message.ClientMessage.PlayerLookMessage;
import explosions.server.entity.EntityUtility;
import explosions.server.entity.ItemControl;
import explosions.server.entity.PlayerControl;
import explosions.server.message.ServerMessage.AudioMessage;
import explosions.server.message.ServerMessage.RemoveMessage;
import explosions.server.message.ServerMessage.SpawnMessage;
import explosions.server.message.ServerMessage.UpdateLocationMessage;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Random;
import java.util.concurrent.Callable;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Roland Fuller
 */
public class ServerAppState extends AppState implements Broadcaster, PhysicsCollisionListener, EntityUtility, WorldUtility, ConnectionListener, MessageListener<HostedConnection> {

    private static final Logger LOGGER = Logger.getLogger(ServerAppState.class.getName());

    static {
        LOGGER.setLevel(Level.OFF);
    }
    private BulletAppState bulletAppState;
    private ServerFactory serverFactory;
    private Server myServer;
    private final String port;
    private Queue<NetworkMessage> messageQueue;

    public ServerAppState(String port) {
        this.port = port;
        messageQueue = new LinkedList<>();
    }

    @Override
    public void initialize(AppStateManager stateManager, Application app) {
        super.initialize(stateManager, app);
        bulletAppState = new BulletAppState();
        stateManager.attach(bulletAppState);
        bulletAppState.getPhysicsSpace().addCollisionListener(this);
        serverFactory = new ServerFactory(this, this, bulletAppState, this, rootNode);
        bulletAppState.setDebugEnabled(true);
        initNetwork();
    }

    @Override
    protected void initNetwork() {
        try {
            myServer = Network.createServer(Integer.parseInt(port));
            myServer.addMessageListener(this, NetworkMessage.class);
            myServer.addMessageListener(this, SpawnMessage.class);
            myServer.addMessageListener(this, ChatMessage.class);
            myServer.addMessageListener(this, PlayerInputMessage.class);
            myServer.addMessageListener(this, PlayerLookMessage.class);
            myServer.addMessageListener(this, UpdateLocationMessage.class);
            myServer.addConnectionListener(this);
            myServer.start();
        } catch (Exception ex) {
            LOGGER.log(Level.SEVERE, "Network failed to initialise! " + ex.getMessage());
        }
    }

    public void startGame(String filePath) {
        serverFactory.makeWorld((Node) app.getAssetManager().loadModel(filePath));
        serverFactory.makeChain(new Vector3f(0, 10, 0), 8);
    }

    /**
     * This is the method implemented by the PhysicsCollisionListener interface
     * which is automatically called by the physics engine any time two physics
     * objects touch
     *
     * @param event
     */
    @Override
    public void collision(PhysicsCollisionEvent event) {
        // We use our resolve method to see if either node A or node B involved in the collision
        if (event.getNodeA() != null && event.getNodeB() != null) {
            Spatial player = resolve(PLAYER, event.getNodeA(), event.getNodeB());
            Spatial bullet = resolve(ROCKET, event.getNodeA(), event.getNodeB());
            Spatial ammo = resolve(AMMO, event.getNodeA(), event.getNodeB());

            // Once resolved, we can see which objects we are dealing with.
            if (bullet != null) {
                if (player != null) {
                    player.getControl(PlayerControl.class).hurt(25);
                }
                die(bullet);
            }

            if (ammo != null) {
                if (player != null) {
                    player.getControl(PlayerControl.class).giveAmmo(25);
                    ammo.getControl(ItemControl.class).take();
                    die(ammo);
                }
            }
        }
    }

    /**
     * A simple utility method which checks to see which of two nodes has the
     * name we're looking for
     *
     * @param name The name we're testing for
     * @param nodeA the first node involved in the collision
     * @param nodeB the second node involved in the collision
     * @return the node that had the name we were looking for
     */
    private Spatial resolve(String name, Spatial nodeA, Spatial nodeB) {
        if (nodeA.getName().equals(name)) {
            return nodeA;
        } else if (nodeB.getName().equals(name)) {
            return nodeB;
        }
        return null;
    }

    @Override
    public void connectionAdded(Server server, final HostedConnection conn) {
        app.enqueue(new Callable<Void>() {
            @Override
            public Void call() throws Exception {
                // find a spawn point in the world and pick one at random
                final Vector3f spawn = getSpawnPoint();
                Node human = serverFactory.makeHuman(spawn);
                int id = human.getUserData(ID);
                conn.setAttribute(ID, id);
                entityMap.put(id, human);
                conn.send(new SpawnMessage(id, PLAYER, spawn, true));

                myServer.broadcast(Filters.notIn(conn), new SpawnMessage(id, PLAYER, spawn));

                final HostedConnection guy = conn;

                rootNode.depthFirstTraversal(new SceneGraphVisitor() {
                    @Override
                    public void visit(Spatial spatial) {
                        boolean sendable = true;
                        if (spatial.getName() != null) {
                            String objectType = "";
                            switch (spatial.getName()) {
                                case MAP:
                                    objectType = MAP;
                                    break;
                                case ROCKET:
                                    objectType = ROCKET;
                                    break;
                                case PLAYER:
                                    objectType = PLAYER;
                                    break;
                                case AMMO:
                                    objectType = AMMO;
                                    break;
                                default:
                                    sendable = false;
                                    break;
                            }
                            if (sendable) {
                                guy.send(new SpawnMessage((int) spatial.getUserData(ID), objectType, spatial.getLocalTranslation()));
                            }
                        }
                    }
                });
                myServer.broadcast(new AudioMessage(id, "Sound/Effects/Bang.wav"));
                return null;
            }
        });
    }

    private Vector3f getSpawnPoint() {
        Vector3f location = new Vector3f();
        List<Spatial> spawnPoints = GameObjectFinder.FindByName(rootNode, Factory.PLAYER_SPAWNER);
        return spawnPoints.get(new Random().nextInt(spawnPoints.size())).getLocalTranslation();
    }

    /**
     * Send out a network message to all clients.
     *
     * @param networkMessage
     */
    @Override
    public void send(final NetworkMessage networkMessage) {
        messageQueue.add(networkMessage);
    }

    public void handleMessage(ChatMessage chatMessage) {
    }

    public void handleMessage(PlayerInputMessage playerInputMessage) {
        // Issue #2 hopefully solved by looking at map instead of searching for entity
        if (entityMap.containsKey(playerInputMessage.id)) {
            PlayerControl pc = entityMap.get(playerInputMessage.id).getControl(PlayerControl.class);
            pc.action(playerInputMessage.command, playerInputMessage.pressed);
        }
    }

    public void handleMessage(PlayerLookMessage playerLookMessage) {
        // Issue #2 hopefully solved by looking at map instead of searching for entity
        if (entityMap.containsKey(playerLookMessage.id)) {
            PlayerControl pc = entityMap.get(playerLookMessage.id).getControl(PlayerControl.class);
            pc.look(playerLookMessage.lookDirection);
        }
    }

    /**
     * Recieved a message from the client. Process it via visitor pattern.
     *
     * @param source
     * @param message
     */
    @Override
    public void messageReceived(HostedConnection source, final Message message) {
        app.enqueue(new Callable<Void>() {
            @Override
            public Void call() throws Exception {
                ((ClientMessage) message).visit(ServerAppState.this);
                return null;
            }
        });
    }

    @Override
    public void rayCast(Vector3f from, Vector3f to, PhysicsCollisionObject sender, RayMessage rayMessage) {
        to = from.add(to.mult(rayMessage.getRange()));
        List<PhysicsRayTestResult> results = new ArrayList<>();
        bulletAppState.getPhysicsSpace().rayTest(from, to, results);
        for (int i = results.size() - 1; i >= 0; i--) {
            if (i < rayMessage.getPenetration()) {
                if (results.get(i).getCollisionObject().getUserObject() != null) {
                    rayHit((Spatial) results.get(i).getCollisionObject().getUserObject(), rayMessage);
                }
            }
        }
    }

    private void rayHit(Spatial spatial, RayMessage rayMessage) {
        PlayerControl pc = spatial.getControl(PlayerControl.class);
        if (pc != null) {
            pc.hit(rayMessage);
        }
    }

    @Override
    public void die(Spatial spatial) {
        send(new RemoveMessage((int) spatial.getUserData(ID)));
        bulletAppState.getPhysicsSpace().removeAll(spatial);
        spatial.removeFromParent();
    }

    @Override
    public void connectionRemoved(Server server, HostedConnection conn) {
        die(entityMap.get(conn.getAttribute(ID)));
        entityMap.remove(conn.getAttribute(ID));
    }

    @Override
    public void cleanup() {
        super.cleanup();
        myServer.close();
    }

    @Override
    public void update(float tpf) {
        super.update(tpf);
        for (NetworkMessage networkMessage : messageQueue) {
            myServer.broadcast(networkMessage);
        }
        messageQueue.clear();
    }

    @Override
    public void rag(Spatial spatial) {
        Spatial model = app.getAssetManager().loadModel("Models/Oto/Oto.mesh.xml");
        model.scale(0.4f);
        model.setLocalTranslation(spatial.getLocalTranslation().add(new Vector3f(0, 3, 0)));
        final KinematicRagdollControl kinematicRagdollControl = new KinematicRagdollControl();
        model.addControl(kinematicRagdollControl);
        kinematicRagdollControl.setRagdollMode();
        rootNode.attachChild(model);
        bulletAppState.getPhysicsSpace().add(model);
        die(spatial);
    }

}
