package explosions.server.database;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author Roland Fuller
 */
public class StringParser implements Parser<String> {

    @Override
    public String parse(ResultSet resultSet) throws SQLException {
        StringBuilder sb = new StringBuilder();
        while (resultSet.next()) {
            for (int i = 0; i < resultSet.getMetaData().getColumnCount(); i++) {
                sb.append(resultSet.getObject(i));
            }
            sb.append("\n");
        }
        return sb.toString();
    }
}
