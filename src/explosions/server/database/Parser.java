package explosions.server.database;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author Roland Fuller
 */
public interface Parser<T> {

    T parse(ResultSet resultSet) throws SQLException;
}
