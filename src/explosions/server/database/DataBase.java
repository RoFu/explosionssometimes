package explosions.server.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * database connectivity
 *
 * @author Roland Fuller
 */
public class DataBase {

    private static final String USER_NAME = "root";
    private static final String PASSWORD = "";
    private static final String HOST = "localhost";
    private static final String PORT = "3306";
    private static final String DB_NAME = "explosions";
    private static final Logger logger;

    static {
        logger = Logger.getLogger(DataBase.class.getName());
        logger.setLevel(Level.SEVERE);
    }

    /**
     * Private constructor so we can't instantiate ... this is a static utility class
     */
    private DataBase() {
    }

    /**
     * Create a connection to the database with the default settings
     * @throws SQLException
     */
    private static Connection getConnection() throws SQLException {
        Properties connectionProps = new Properties();
        connectionProps.put("user", USER_NAME);
        connectionProps.put("password", PASSWORD);
        return DriverManager.getConnection(
                "jdbc:mysql://"
                + HOST
                + ":" + PORT + "/" + DB_NAME,
                connectionProps);
    }

    /**
     * Get a list of objects from the database in a generic way
     * @param <T> type of objects to create from database
     * @param query the SQL query to execute
     * @param parser the parser object to translate the record into an object
     * @return a list of objects of the specified type
     */
    public static <T> List<T> getObjects(String query, Parser<T> parser) {
        List<T> objects = new LinkedList<>();
        try (Connection connection = getConnection()) {
            Statement stmt = connection.createStatement();
            ResultSet rs = stmt.executeQuery(query);
            while (rs.next()) {
                objects.add(parser.parse(rs));
            }
            connection.close();
        } catch (SQLException ex) {
            logger.log(Level.SEVERE, null, ex);
        }
        return objects;
    }
}
