package explosions.server.weapons;

/**
 *
 * @author Roland Fuller
 */
public class Pistol extends Weapon {

    public Pistol(Shooter shooter, Cache cache) {
        super("Pistol", shooter, 0.5f, 2, 3, 10, cache);
    }
}
