package explosions.server.weapons;

/**
 *
 * @author Roland Fuller
 */
abstract public class Weapon {

    private enum WeaponState {
        READY, FIRE, CHAMBER, RELOAD, RELOADING
    };
    private WeaponState state = WeaponState.READY;
    private final float refireTime;
    private float refireTimer;
    private final float reloadTime;
    private float reloadTimer;
    private int ammo;
    private int loadAmmo;
    private final int ammoCapacity;
    private final Cache cache;
    private final Shooter shooter;
    private String name;

    public Weapon(String name, Shooter shooter, float refireTime, float reloadTime, int ammo, int ammoCapacity, Cache cache) {
        this.name = name;
        this.refireTime = refireTime;
        this.reloadTime = reloadTime;
        this.ammo = ammo;
        this.ammoCapacity = ammoCapacity;
        this.cache = cache;
        this.shooter = shooter;
    }

    public void fire() {
        if (state == WeaponState.READY) {
            state = WeaponState.FIRE;
        }
    }

    public void reload() {
        if (state != WeaponState.RELOADING) {
            state = WeaponState.RELOAD;
        }
    }

    public void update(float tpf) {
        switch (state) {
            case FIRE:
                if (ammo > 0) {
                    ammo--;
                    shooter.shoot();
                    state = WeaponState.CHAMBER;
                } else {
                    reload();
                }
                break;
            case CHAMBER:
                refireTimer += tpf;
                if (refireTimer >= refireTime) {
                    refireTimer = 0;
                    state = WeaponState.READY;
                }
                break;
            case RELOAD:
                if (ammo < ammoCapacity) {
                    if (cache.peekAmmo()) {
                        state = WeaponState.RELOADING;
                        loadAmmo = cache.getAmmo(ammoCapacity - ammo);
                    }
                } else {
                    state = WeaponState.READY;
                }
                break;
            case RELOADING:
                reloadTimer += tpf;
                if (reloadTimer >= reloadTime) {
                    reloadTimer = 0;
                    refireTimer = 0; // avoid rapid fire bug
                    ammo += loadAmmo;
                    loadAmmo = 0;
                    state = WeaponState.READY;
                }
                break;
        }
    }
}
