package explosions.server.weapons;

/**
 *
 * @author Roland Fuller
 */
public class Cache {

    private int ammo;

    public Cache(int ammo) {
        this.ammo = ammo;
    }

    public int getAmmo(int amount) {
        if (ammo >= amount) {
            ammo -= amount;
            return amount;
        } else {
            amount = ammo;
            ammo = 0;
            return amount;
        }
    }

    public boolean peekAmmo() {
        return ammo > 0;
    }

    public void giveAmmo(int i) {
        ammo += i;
    }

}
