package explosions.server.weapons;

import explosions.server.ServerFactory;
import explosions.server.entity.PlayerControl;

/**
 *
 * @author Roland Fuller
 */
public class RocketShooter extends Shooter {

    public RocketShooter(ServerFactory serverFactory, PlayerControl playerControl) {
        super(serverFactory, playerControl);
    }

    @Override
    public void shoot() {
        serverFactory.makeRocket(playerControl.getLocation(), playerControl.getViewDirection(), playerControl.getHeight() / 2);
    }
}
