package explosions.server.weapons;

import explosions.server.ServerFactory;
import explosions.server.entity.PlayerControl;

/**
 *
 * @author Roland Fuller
 */
public abstract class Shooter {

    protected ServerFactory serverFactory;
    protected PlayerControl playerControl;

    public Shooter(ServerFactory serverFactory, PlayerControl playerControl) {
        this.serverFactory = serverFactory;
        this.playerControl = playerControl;
    }

    public abstract void shoot();
}
