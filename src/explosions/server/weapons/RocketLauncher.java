package explosions.server.weapons;

/**
 *
 * @author Roland Fuller
 */
public class RocketLauncher extends Weapon {

    public RocketLauncher(Shooter shooter, Cache cache) {
        super("Rocket Launcher", shooter, 1, 2, 10, 100, cache);
    }

}
