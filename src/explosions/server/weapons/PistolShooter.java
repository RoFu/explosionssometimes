package explosions.server.weapons;

import explosions.server.ServerFactory;
import explosions.server.entity.PlayerControl;

/**
 *
 * @author Roland Fuller
 */
public class PistolShooter extends Shooter {

    public PistolShooter(ServerFactory serverFactory, PlayerControl playerControl) {
        super(serverFactory, playerControl);
    }

    @Override
    public void shoot() {
        serverFactory.makeBullet(playerControl.getLocation(), playerControl.getViewDirection(), playerControl.getRigidBody(), playerControl.getHeight() / 2);
    }

}
