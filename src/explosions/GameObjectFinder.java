package explosions;

import com.jme3.scene.SceneGraphVisitor;
import com.jme3.scene.Spatial;
import com.jme3.scene.control.Control;
import static explosions.Factory.ID;
import java.util.ArrayList;
import java.util.List;

/**
 * Goes searching through the node tree to find spatials which conform to some
 * kind of search condition.
 *
 * Provides some utility methods for common searches.
 *
 * @author Roland Fuller
 */
public class GameObjectFinder<T> implements SceneGraphVisitor {

    /**
     * What any condition must look like. Condition is just a single checking
     * method which takes a spatial and subjects it to whatever filter logic we
     * like, then returns an object of some sort. This return object is generic,
     * so we can grab whatever data we like depending on the condition.
     *
     * @param <T>
     */
    public static interface Condition<T> {

        public T check(Spatial spatial);
    }

    private List<T> matches; // the list of objects to return that were found as a result
    private Condition<T> condition; // the condition to match a spatial against

    /**
     * Constructor sets up the list and accepts a condition
     *
     * @param condition
     */
    public GameObjectFinder(Condition condition) {
        this.condition = condition;
        matches = new ArrayList<T>();
    }

    /**
     * The override for the SceneGraphVisitor. Adds to our match list any result
     * the condition found against the spatial. This method is called for all
     * spatials in the tree and acts as a filter to get us the spatials we want.
     *
     * @param spatial
     */
    @Override
    public void visit(Spatial spatial) {
        final T check = condition.check(spatial);
        if (check != null) {
            matches.add(check);
        }
    }

    /**
     *
     * @return whatever objects were discovered
     */
    public List<T> getMatches() {
        //while (matches.remove(null)); // clear the list of any null entries as they aren't useful
        return matches;
    }

    /**
     * Get all the spatials which have a particular id used to track down a
     * specific game object would be more efficient to store a hash map of ids
     * to spatials but require more management when objects are
     * created/destroyed
     *
     * @param root the node to start searching from
     * @param id the id to locate
     * @return all the spatials
     */
    public static List<Spatial> FindById(Spatial root, final int id) {
        return search(root, new Condition<Spatial>() {
            @Override
            public Spatial check(Spatial spatial) {
                final Object spatialID = spatial.getUserData(ID);
                if (spatialID != null) {
                    if (id == (int) spatialID) {
                        return spatial;
                    }
                }
                return null;
            }
        }).getMatches();
    }

    /**
     * Find all the spatials which have a given controller type attached.
     *
     * @param root
     * @param control
     * @return
     */
    public static List<Control> FindByControllerType(Spatial root, final Class control) {
        return search(root, new Condition<Control>() {
            @Override
            public Control check(Spatial spatial) {
                for (int i = 0; i < spatial.getNumControls(); i++) {
                    if (spatial.getControl(i).getClass().equals(control)) {
                        return spatial.getControl(i);
                    }
                }
                return null;
            }
        }).getMatches();
    }

    /**
     * Find spatials of a given type (typically node or spatial)
     *
     * @param root
     * @param type
     * @return
     */
    public static List<Spatial> FindByType(Spatial root, final Class type) {
        return search(root, new Condition<Spatial>() {
            @Override
            public Spatial check(Spatial spatial) {
                if (spatial.getClass().equals(type)) {
                    return spatial;
                } else {
                    return null;
                }
            }
        }).getMatches();
    }

    /**
     * Find spatials of a given type (typically node or spatial)
     *
     * @param root
     * @param name
     * @param type
     * @return
     */
    public static List<Spatial> FindByName(Spatial root, final String name) {
        return search(root, new Condition<Spatial>() {
            @Override
            public Spatial check(Spatial spatial) {
                if ((spatial.getName() != null) && (spatial.getName().toUpperCase().contains(name))) {
                    return spatial;
                } else {
                    return null;
                }
            }
        }).getMatches();
    }

    private static GameObjectFinder search(Spatial root, Condition condition) {
        GameObjectFinder gameObjectFinder = new GameObjectFinder(condition);
        root.depthFirstTraversal(gameObjectFinder);
        return gameObjectFinder;
    }
}
