package explosions;

/**
 *
 * @author Roland Fuller
 */
public interface Broadcaster {

    void send(NetworkMessage networkMessage);

}
