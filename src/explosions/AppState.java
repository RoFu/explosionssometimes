package explosions;

import com.jme3.app.Application;
import com.jme3.app.SimpleApplication;
import com.jme3.app.state.AbstractAppState;
import com.jme3.app.state.AppStateManager;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Roland Fuller
 */
public abstract class AppState extends AbstractAppState {

    protected Node rootNode;
    protected SimpleApplication app;
    protected Map<Integer, Spatial> entityMap;

    @Override
    public void initialize(AppStateManager stateManager, Application app) {
        super.initialize(stateManager, app);
        this.app = (SimpleApplication) app;
        this.app.getRootNode().attachChild(rootNode = new Node());
        entityMap = new HashMap<>();
    }

    protected abstract void initNetwork() throws Exception;

}
