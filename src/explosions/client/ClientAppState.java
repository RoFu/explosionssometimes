package explosions.client;

import com.jme3.app.Application;
import com.jme3.app.state.AppStateManager;
import com.jme3.audio.AudioNode;
import com.jme3.input.KeyInput;
import com.jme3.input.MouseInput;
import com.jme3.input.controls.KeyTrigger;
import com.jme3.input.controls.MouseButtonTrigger;
import com.jme3.math.Vector3f;
import com.jme3.network.Client;
import com.jme3.network.ClientStateListener;
import com.jme3.network.ErrorListener;
import com.jme3.network.Message;
import com.jme3.network.MessageListener;
import com.jme3.network.Network;
import com.jme3.scene.Spatial;
import explosions.AppState;
import explosions.Broadcaster;
import static explosions.Factory.AMMO;
import static explosions.Factory.MAP;
import static explosions.Factory.PLAYER;
import static explosions.Factory.ROCKET;
import explosions.GameObjectFinder;
import explosions.NetworkMessage;
import static explosions.client.entity.PlayerControl.BACKWARD;
import static explosions.client.entity.PlayerControl.FORWARD;
import static explosions.client.entity.PlayerControl.JUMP;
import static explosions.client.entity.PlayerControl.LEFT;
import static explosions.client.entity.PlayerControl.RELOAD;
import static explosions.client.entity.PlayerControl.RIGHT;
import static explosions.client.entity.PlayerControl.SHOOT;
import static explosions.client.entity.PlayerControl.SWITCH;
import explosions.client.entity.UpdateControl;
import explosions.client.message.ClientMessage.ChatMessage;
import explosions.client.message.ClientMessage.PlayerInputMessage;
import explosions.client.message.ClientMessage.PlayerLookMessage;
import explosions.server.message.ServerMessage.AnimationMessage;
import explosions.server.message.ServerMessage.AudioMessage;
import explosions.server.message.ServerMessage.RemoveMessage;
import explosions.server.message.ServerMessage.SpawnMessage;
import explosions.server.message.ServerMessage.UpdateLocationMessage;
import java.util.concurrent.Callable;

/**
 *
 * @author Roland Fuller
 */
public class ClientAppState extends AppState implements Broadcaster, MessageListener<Client>, ErrorListener<Object>, ClientStateListener {

    private ClientFactory clientFactory;
    private Client myClient;
    private final String ipAddress;
    private final int portNumber;

    public ClientAppState(String ipAddress, int portNumber) throws Exception {
        this.ipAddress = ipAddress;
        this.portNumber = portNumber;
        initNetwork();
    }

    @Override
    public void initialize(AppStateManager stateManager, Application app) {
        super.initialize(stateManager, app);
        clientFactory = new ClientFactory(app.getCamera(), app.getAssetManager(), app.getInputManager(), this, rootNode);
        initKeys();
    }

    @Override
    protected void initNetwork() throws Exception {
        myClient = Network.connectToServer(ipAddress, portNumber);
        myClient.addMessageListener(this, NetworkMessage.class);
        myClient.addMessageListener(this, SpawnMessage.class);
        myClient.addMessageListener(this, ChatMessage.class);
        myClient.addMessageListener(this, PlayerInputMessage.class);
        myClient.addMessageListener(this, PlayerLookMessage.class);
        myClient.addMessageListener(this, UpdateLocationMessage.class);
        myClient.addMessageListener(this, AudioMessage.class);
        myClient.addMessageListener(this, AnimationMessage.class);
        myClient.addMessageListener(this, RemoveMessage.class);
        myClient.addClientStateListener(this);
        myClient.addErrorListener(this);
        myClient.start();
    }

    private void initKeys() {
        app.getInputManager().addMapping(JUMP, new KeyTrigger(KeyInput.KEY_SPACE));
        app.getInputManager().addMapping(FORWARD, new KeyTrigger(KeyInput.KEY_W));
        app.getInputManager().addMapping(BACKWARD, new KeyTrigger(KeyInput.KEY_S));
        app.getInputManager().addMapping(LEFT, new KeyTrigger(KeyInput.KEY_A));
        app.getInputManager().addMapping(RIGHT, new KeyTrigger(KeyInput.KEY_D));
        app.getInputManager().addMapping(SHOOT, new MouseButtonTrigger(MouseInput.BUTTON_LEFT));
        app.getInputManager().addMapping(RELOAD, new KeyTrigger(KeyInput.KEY_E));
        app.getInputManager().addMapping(SWITCH, new KeyTrigger(KeyInput.KEY_Q));
    }

    @Override
    public void send(final NetworkMessage networkMessage) {
        app.enqueue(new Callable<Void>() {
            @Override
            public Void call() throws Exception {
                myClient.send(networkMessage);
                return null;
            }
        });
    }

    @Override
    public void messageReceived(Client source, final Message message) {
        app.enqueue(new Callable<Void>() {
            @Override
            public Void call() throws Exception {
                if (message instanceof ChatMessage) {
                    ChatMessage helloMessage = (ChatMessage) message;
                } else if (message instanceof UpdateLocationMessage) {
                    UpdateLocationMessage ulm = (UpdateLocationMessage) message;
                    updateGameObjectLocation(ulm.id, ulm.location);
                } else if (message instanceof SpawnMessage) {
                    SpawnMessage sm = (SpawnMessage) message;
                    switch (sm.gameObject) {
                        case MAP:
                            clientFactory.makeWorld(sm.id);
                            break;
                        case PLAYER:
                            clientFactory.makePlayer(sm.id, sm.location, sm.isPlayer);
                            break;
                        case ROCKET:
                            clientFactory.makeRocket(sm.id, sm.location);
                            break;
                        case AMMO:
                            clientFactory.makeAmmo(sm.id, sm.location);
                            break;
                    }
                } else if (message instanceof AudioMessage) {
                    AudioMessage am = (AudioMessage) message;
                    AudioNode an = (AudioNode) rootNode.getChild(am.effect);
                    an.playInstance();
                } else if (message instanceof AnimationMessage) {
                    AnimationMessage am = (AnimationMessage) message;
                    updateGameObjectAnimation(am.id, am.animation);
                } else if (message instanceof RemoveMessage) {
                    RemoveMessage rm = (RemoveMessage) message;
                    removeGameObject(rm.id);
                }
                return null;
            }
        });
    }

    private void removeGameObject(final int id) {
        for (Spatial spatial : GameObjectFinder.FindById(rootNode, id)) {
            UpdateControl ncc = spatial.getControl(UpdateControl.class);
            ncc.timesUp();
        }
    }

    private void updateGameObjectAnimation(final int id, final String animation) {
        for (Spatial spatial : GameObjectFinder.FindById(rootNode, id)) {
            UpdateControl ncc = spatial.getControl(UpdateControl.class);
            ncc.playAnimation(animation);
        }
    }

    private void updateGameObjectLocation(final int id, final Vector3f location) {
        for (Spatial spatial : GameObjectFinder.FindById(rootNode, id)) {
            UpdateControl ncc = spatial.getControl(UpdateControl.class);
            ncc.move(location);
        }
    }

    @Override
    public void cleanup() {
        super.cleanup();
        if (myClient != null) {
            myClient.close();
        }
    }

    @Override
    public void handleError(Object source, Throwable t) {
    }

    @Override
    public void clientConnected(Client c) {
    }

    @Override
    public void clientDisconnected(Client c, DisconnectInfo info) {
    }
}
