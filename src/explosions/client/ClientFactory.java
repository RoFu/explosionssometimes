package explosions.client;

import com.jme3.animation.AnimChannel;
import com.jme3.animation.AnimControl;
import com.jme3.asset.AssetManager;
import com.jme3.audio.AudioData;
import com.jme3.audio.AudioNode;
import com.jme3.effect.ParticleEmitter;
import com.jme3.effect.ParticleMesh.Type;
import com.jme3.input.InputManager;
import com.jme3.light.DirectionalLight;
import com.jme3.light.PointLight;
import com.jme3.material.Material;
import com.jme3.math.ColorRGBA;
import com.jme3.math.Vector2f;
import com.jme3.math.Vector3f;
import com.jme3.renderer.Camera;
import com.jme3.scene.Geometry;
import com.jme3.scene.Mesh;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import com.jme3.scene.control.Control;
import com.jme3.scene.control.LightControl;
import com.jme3.scene.shape.Box;
import com.jme3.scene.shape.Sphere;
import com.jme3.texture.Texture;
import com.jme3.texture.Texture.WrapMode;
import explosions.Broadcaster;
import explosions.Factory;
import explosions.GameObjectFinder;
import explosions.client.entity.PlayerControl;
import static explosions.client.entity.PlayerControl.BACKWARD;
import static explosions.client.entity.PlayerControl.FORWARD;
import static explosions.client.entity.PlayerControl.JUMP;
import static explosions.client.entity.PlayerControl.LEFT;
import static explosions.client.entity.PlayerControl.RELOAD;
import static explosions.client.entity.PlayerControl.RIGHT;
import static explosions.client.entity.PlayerControl.SHOOT;
import static explosions.client.entity.PlayerControl.SWITCH;
import explosions.client.entity.UpdateControl;
import java.util.List;

/**
 *
 * @author Roland Fuller
 */
public class ClientFactory extends Factory {

    public static final String PARTICLE_EMITTER = "Emitter";
    public static final String TEXTURE = "Texture";

    private AssetManager assetManager;
    private InputManager inputManager;
    private Camera camera;

    public ClientFactory(Camera camera, AssetManager assetManager, InputManager inputManager, Broadcaster broadcaster, Node rootNode) {
        super(rootNode, broadcaster);
        this.assetManager = assetManager;
        this.inputManager = inputManager;
        this.camera = camera;
    }

    private Geometry giveModel(Mesh mesh, Node node) {
        return giveGeneratedModel(mesh, node, Vector3f.ZERO);
    }

    private void makeViewModel(Node container) {
        giveGeneratedModel(new Box(0.25f, 0.25f, 0.25f), container, new Vector3f(0, 3f, 1f));
    }

    private Geometry giveGeneratedModel(Mesh mesh, Node node, Vector3f offset) {
        Geometry geom = new Geometry("Box", mesh);
        Material mat = new Material(assetManager, "Common/MatDefs/Light/Lighting.j3md");

        mesh.scaleTextureCoordinates(new Vector2f(32, 32));
        final Texture diffuseTexture = assetManager.loadTexture("Textures/Terrain/Rock/Rock.PNG");
        mat.setTexture("DiffuseMap", diffuseTexture);

        final Texture normalTexture = assetManager.loadTexture("Textures/Terrain/Rock/Rock_normal.png");
        // normalTexture.setMagFilter(Texture.MagFilter.Bilinear);
        mat.setTexture("NormalMap", normalTexture);

        mat.getTextureParam("DiffuseMap").getTextureValue().setWrap(WrapMode.Repeat);
        mat.getTextureParam("NormalMap").getTextureValue().setWrap(WrapMode.Repeat);

        geom.setMaterial(mat);
        node.attachChild(geom);
        geom.setLocalTranslation(offset);
        return geom;
    }

    private Spatial giveAnimatedModel(String path, Node node, UpdateControl updateControl, Vector3f offset) {
        Spatial spatial = assetManager.loadModel(path);
        List<Control> matches = GameObjectFinder.FindByControllerType(spatial, AnimControl.class);
        spatial.setLocalTranslation(offset);
        if (matches.size() > 0) {
            AnimControl animControl = (AnimControl) matches.get(0);
            AnimChannel animChannel = animControl.createChannel();
            updateControl.setAnimChannel(animChannel);
        }
        spatial.scale(0.4f);
        node.attachChild(spatial);
        return spatial;
    }

    public void makeWorld(int id) {
        loadAudio();

        Node container = (Node) assetManager.loadModel("Maps/map.j3o");
        rootNode.attachChild(container);

        DirectionalLight directionalLight = new DirectionalLight(new Vector3f(0.1324f, -1f, -0.023f), ColorRGBA.Yellow);
        rootNode.addLight(directionalLight);

        rootNode.addLight(new PointLight(new Vector3f(0, 16, 0), ColorRGBA.Blue, 1000));
    }

    public void makeRocket(int id, Vector3f location) {
        Node container = makeCore(location, ROCKET, id);
        container.addControl(new UpdateControl());
        giveModel(new Sphere(32, 32, 0.25f), container);
        final PointLight pointLight = new PointLight(location, ColorRGBA.Orange, 10);
        LightControl lightControl = new LightControl(pointLight);
        container.addLight(pointLight);
        container.addControl(lightControl);
        container.attachChild(makeSmoke());
        container.attachChild(makeFire());
    }

    public void makeAmmo(int id, Vector3f location) {
        Node container = makeCore(location, AMMO, id);
        container.addControl(new UpdateControl());
        giveModel(new Box(0.5f, 0.5f, 0.5f), container);
    }

    public void makePlayer(int id, Vector3f location, boolean self) {
        Node container = makeCore(location, PLAYER, id);
        UpdateControl updateControl = null;
        if (self) {
            updateControl = new PlayerControl(broadcaster, camera);
            inputManager.addListener((PlayerControl) updateControl, new String[]{JUMP, FORWARD, BACKWARD, LEFT, RIGHT, SHOOT, SWITCH, RELOAD});
            makeViewModel(container);
        } else {
            updateControl = new UpdateControl();
        }
        container.addControl(updateControl);
        giveAnimatedModel("Models/Oto/Oto.mesh.xml", container, updateControl, new Vector3f(0, 2, 0));
    }

    private void loadAudio() {
        loadSound("Sound/Effects/Bang.wav");
        loadSound("Sound/Effects/Beep.ogg");
        loadSound("Sound/Effects/Foot steps.ogg");
        loadSound("Sound/Effects/Gun.wav");
        loadSound("Sound/Effects/kick.wav");
    }

    private void loadSound(String path) {
        final AudioNode audioNode = new AudioNode(assetManager, path, AudioData.DataType.Buffer);
        audioNode.setName(path);
        rootNode.attachChild(audioNode);
    }

    private ParticleEmitter makeParticleEffect(int particleCount, String texture) {
        ParticleEmitter particleEmitter = new ParticleEmitter(PARTICLE_EMITTER, Type.Triangle, particleCount);
        Material material = new Material(assetManager, "Common/MatDefs/Misc/Particle.j3md");
        material.setTexture(TEXTURE, assetManager.loadTexture(texture));
        particleEmitter.setMaterial(material);
        rootNode.attachChild(particleEmitter);
        return particleEmitter;
    }

    private ParticleEmitter makeFire() {
        ParticleEmitter fire = makeParticleEffect(10, "Effects/Explosion/flame.png");
        fire.setImagesX(2);
        fire.setImagesY(2);
        fire.setEndColor(new ColorRGBA(1f, 1f, 0f, 1f));
        fire.setStartColor(new ColorRGBA(1f, 0, 0, 1f));
        fire.setStartSize(1f);
        fire.setEndSize(0.5f);
        fire.setGravity(0, 0, 0);
        fire.setLowLife(0.1f);
        fire.setHighLife(0.1f);
        fire.setParticlesPerSec(10);
        return fire;
    }

    private ParticleEmitter makeSmoke() {
        ParticleEmitter smoke = makeParticleEffect(50, "Effects/Explosion/flame.png");
        smoke.setImagesX(2);
        smoke.setImagesY(2);
        smoke.setEndColor(new ColorRGBA(0.1f, 0.1f, 0.1f, 0.01f));
        smoke.setStartColor(new ColorRGBA(0.5f, 0.5f, 0.5f, 0.1f));
        smoke.setStartSize(1f);
        smoke.setEndSize(30f);
        smoke.setGravity(0, 0, 0);
        smoke.setLowLife(1f);
        smoke.setHighLife(6f);
        smoke.setParticlesPerSec(30);
        smoke.getParticleInfluencer().setVelocityVariation(0.3f);
        return smoke;
    }

}
